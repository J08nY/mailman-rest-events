""""""
from numbers import Number

from mailman.core.api import API31
from mailman.email.message import Message
from mailman.interfaces.address import IAddress
from mailman.interfaces.configuration import IConfiguration
from mailman.interfaces.domain import IDomain
from mailman.interfaces.mailinglist import IMailingList
from mailman.interfaces.member import IMember
from mailman.interfaces.user import IUser


def serialize(obj):
    if isinstance(obj, str):
        return obj

    elif isinstance(obj, Number) or obj is None:
        return str(obj)

    elif IAddress.providedBy(obj):
        return obj.email

    elif IDomain.providedBy(obj):
        return obj.mail_host

    elif IMailingList.providedBy(obj):
        return obj.list_id

    elif IMember.providedBy(obj):
        return API31.from_uuid(obj.member_id)

    elif IUser.providedBy(obj):
        return API31.from_uuid(obj.user_id)

    elif IConfiguration.providedBy(obj):
        return None # not sure what to do here

    elif isinstance(obj, Message):
        return str(obj) # not sure if this is the best...

    elif callable(obj):
        return obj.__name__

    elif isinstance(obj, list):
        return [serialize(sub) for sub in obj]

    elif isinstance(obj, dict):
        # go key by key return dict recursively serialized
        return {key: serialize(value) for key, value in obj.items()}
    else:
        # go non-private attr by attr, return dict recursively serialized
        return {key: serialize(getattr(obj, key, None))
                for key in dir(obj)
                if not key.startswith('_') and
                    not callable(getattr(obj, key, None))}
