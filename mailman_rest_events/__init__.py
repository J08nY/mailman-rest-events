""""""

import re

import requests
from mailman.config import config as mailman_config
from mailman.config.config import external_configuration
from mailman.interfaces.plugin import IPlugin
from mailman.utilities.string import expand
from mailman_rest_events.serialize import serialize
from public import public
from zope.event import subscribers
from zope.interface import implementer

endpoints = []


@public
@implementer(IPlugin)
class RESTEvents:
    def pre_hook(self):
        global endpoints
        # Load config
        plugin_section = dict(mailman_config.plugin_configs).get(self.name)
        config_path = expand(plugin_section.configuration, None,
                             mailman_config.paths)
        config = external_configuration(config_path)

        # Load all endpoints from config.
        for section in config.sections():
            if not section.startswith('endpoint.'):
                continue
            items = config.items(section)
            endpoint = {}
            for key, value in items:
                if key in ('url', 'api_key', 'events') and value != '':
                    endpoint[key] = value
                else:
                    continue
            if len(endpoint) > 0:
                endpoints.append(endpoint)

        # Now compile regex in endpoint['events']
        for endpoint in endpoints:
            event_regex = endpoint['events']
            endpoint['events'] = re.compile(event_regex)

        subscribers.append(on_event)

    def post_hook(self):
        pass

    def rest_object(self):
        return None


def on_event(event):
    class_name = event.__class__.__name__
    for endpoint in endpoints:
        # This matching could be possibly done in pre_hook (but then all events
        # in Mailman should inherit from a common superclass/interface, so that
        # their class names could be dynamically fetched on pre_hook, regexes
        # matched in there and then only a list of matched class names walked
        # here. For the time being just do it here.
        if endpoint['events'].match(class_name):
            data = dict(name=class_name, event=serialize(event))
            requests.post(endpoint['url'],
                          params=dict(key=endpoint['api_key']),
                          json=data)