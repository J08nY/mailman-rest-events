from setuptools import setup, find_packages

setup(
    name='mailman-rest-events',
    version='0.1',
    description='Mailman REST event dispatch plugin',
    long_description='',
    author='Jan Jancar',
    author_email='johny@neuromancer.sk',
    keywords='email',
    classifiers=[
        'Development Status :: 1 - Planning',
        "Topic :: Communications :: Email :: Mailing List Servers",
        "Programming Language :: Python :: 3",
    ],
    packages=find_packages(),
    include_package_data=True,
    install_requires = [
        'mailman',
        'atpublic',
        'requests',
        'zope.interface',
        'zope.event'
    ]
)
